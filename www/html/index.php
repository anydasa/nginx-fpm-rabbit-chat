<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Chat Example Using STOMP Over WebSockets</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/sockjs-client/0.3.4/sockjs.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">


    <style>
        .chat {list-style: none; margin: 0; padding: 0;}
        .chat li {margin-bottom: 10px; padding-bottom: 5px; border-bottom: 1px dotted #B3A9A9;}
        .panel-body {overflow-y: scroll; height: 250px;}
        .chat li .chat-body p {margin: 0; color: #777777;}
    </style>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-primary">
                <div class="panel-heading" id="accordion">
                    <span class="glyphicon glyphicon-comment"></span> Chat
                </div>
                <div class="panel-collapse" id="collapseOne">
                    <div class="panel-body">
                        <ul class="chat"></ul>
                    </div>
                    <div class="panel-footer">
                        <form class="form-inline">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="msg">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Отправить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script id="msgTmpl" type="text/jquery-tmpl">
    <li class="left clearfix">
        <div class="chat-body clearfix">
            <div class="header">
                <strong class="primary-font">Some user</strong>
                <small class="pull-right text-muted">
                    <span class="glyphicon glyphicon-time"></span> 00:00
                </small>
            </div>
            <p class="message"></p>
        </div>
    </li>
</script>


<script type="text/javascript">

    var ws = new SockJS("http://"+location.hostname+":15674/stomp");
    var client = Stomp.over(ws);

    // RabbitMQ SockJS does not support heartbeats so disable them
    client.heartbeat.outgoing = 0;
    client.heartbeat.incoming = 0;

    client.connect("guest", "guest", onConnect, onError, "/");

    function onConnect() {
        var id = client.subscribe("/exchange/chat", function (d) {

           var $node = $($("#msgTmpl").html());
            $(".chat").append($node);

            $node.find(".message").html(d.body);

            $('.panel-body').animate({scrollTop: $('.panel-body').get(0).scrollHeight});
        });
    }

    function onError(e) {
        alert(e);
        console.log("At First you must declare Rabbit exchange. Go to 'http://"+location.hostname+":8001/declare-exchage.php'");
    }

    $("form").on('submit', function (e) {
        var msg = $("#msg").val();
        client.send('/exchange/chat', {"content-type": "text/plain"}, msg);

        $("#msg").val('');
        e.preventDefault();
    });

</script>

</body>
</html>
