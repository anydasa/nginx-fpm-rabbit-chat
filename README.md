# README #

Simple chat based on RabbitMQ exchange, using rabbitmq web-stamp plugin.


### How do setup? ###

You must have installed docker and docker-compose


build and up containers
```
docker-compose up --build
```
install vendors
```
docker exec chat-fpm composer install
```

open your favorite browser and go to url 
```
http://0.0.0.0:8002/
```
you should will have seen a rabbitmq management page 

declare exchange
```
http://0.0.0.0:8001/declare-exchage.php
```

on page
```
http://0.0.0.0:8001/
```
you will have seen simple chat